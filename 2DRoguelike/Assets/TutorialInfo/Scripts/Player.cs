﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Player : MovingObject {

	public int wallDamage = 1;
	public int pointsPerFood= 10;
	public int pointsPersoda=20;
    public int pointsperBadFood = -10;
	public float restartLevelDelay= 1f;
    public Text foodText;
    public Text scoreText;
    public AudioClip moveSound1;
    public AudioClip moveSound2;
    public AudioClip eatSound1;
    public AudioClip eatSound2;
    public AudioClip drinkSound1;
    public AudioClip drinkSound2;
    public AudioClip enemyAttack1;
    public AudioClip enemyAttack2;
    public AudioClip gameOversound;


	private Animator animator;
	private int food;


	// Use this for initialization
	protected override void Start () {

		animator = GetComponent<Animator> ();
		food = ManagerGame.instance.playerFoodPoints;
        

        foodText.text = "Food: " + food;
       

		base.Start ();
		
	}


	private void OnDisable()
	{
		ManagerGame.instance.playerFoodPoints = food;
	}
	// Update is called once per frame
	void Update () {
		if (!ManagerGame.instance.playersTurn) return;



		int horizontal = 0;
		int vertical = 0;


		horizontal = (int) Input.GetAxisRaw("Horizontal");
		vertical = (int) Input.GetAxisRaw("Vertical");

		if (horizontal != 0)
			vertical = 0;


		if (horizontal != 0 || vertical != 0)
			AttemptMove<Wall> (horizontal, vertical);

	}

	protected override void AttemptMove <T> (int xDir, int yDir)
	{
		food--;
        foodText.text = "Food: " + food;
        base.AttemptMove <T> (xDir, yDir);

		RaycastHit2D hit;
        if (Move (xDir, yDir, out hit))
        {
            SoundManager.instance.RandomizeSfx(moveSound1, moveSound2);
        }
		CheckIfGameOver ();

		ManagerGame.instance.playersTurn = false;
	}

	private void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Exit") {
			Invoke ("Restart", restartLevelDelay);
			enabled = false;
		} else if (other.tag == "Food"){
			food += pointsPerFood;
            foodText.text = "+" + pointsPerFood + " Food: " + food;
            SoundManager.instance.RandomizeSfx(eatSound1, eatSound2);
			other.gameObject.SetActive (false);
		} else if (other.tag == "Soda") {
			food += pointsPersoda;
            foodText.text = "+" + pointsPersoda + " Food: " + food;
            SoundManager.instance.RandomizeSfx(drinkSound1, drinkSound2);
			other.gameObject.SetActive (false);
		} else if (other.tag == "BadFood"){
            food += pointsperBadFood;
            foodText.text = pointsperBadFood + " Food: " + food;
            SoundManager.instance.RandomizeSfx(gameOversound);
            other.gameObject.SetActive(false);
        }

	}


	protected override void OnCantMove <T> (T component)
	{
		Wall hitWall = component as Wall;
		hitWall.DamageWall (wallDamage);
		animator.SetTrigger ("playerchop");
	}

	private void Restart()
	{
		SceneManager.LoadScene (0);
	}

	public void Losefood (int loss)
	{
		animator.SetTrigger ("playerhit");
		food -= loss;
        foodText.text = "-" + loss + "Food: " + food;
		CheckIfGameOver ();
	}
	private void CheckIfGameOver()
	{
		if (food <= 0)
        {
            ManagerGame.instance.GameOver();
            SoundManager.instance.PlaySingle(gameOversound);
            SoundManager.instance.musicSource.Stop();
        }
			
        
	}
}
